from django.core.management import BaseCommand

from linkchecker.linkchecker_engine import LinkChecker


class Command(BaseCommand):

    def add_arguments(self, parser):
        # First commandline argument will be describing
        # application/product title
        parser.add_argument('product_title')
        # Second one takes list of links as argument
        parser.add_argument('links', nargs='+', type=str)

    def handle(self, *args, **options):
        # Handling given variables from commandline and
        # passing them into our application engine
        product_title = options['product_title']
        links = options['links']
        l = LinkChecker(product_title)
        l.get_links(links)
        l.set_data()
