from django.db import models
import django.utils.timezone


class Product(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)


class OffendingItem(models.Model):
    STATUS_DEAD = 0
    STATUS_LIVE = 1
    OI_STATUS_CHOICES = (
        (STATUS_DEAD, 'Dead'),
        (STATUS_LIVE, 'Live'),
    )
    product = models.ForeignKey(Product)
    url = models.CharField(max_length=200, null=True, blank=True)
    status = models.IntegerField(choices=OI_STATUS_CHOICES, null=True, blank=True)
    date_found = models.DateTimeField(
        default=django.utils.timezone.now,
        null=True,
        blank=True)
