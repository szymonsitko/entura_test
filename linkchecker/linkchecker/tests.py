from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase

from .models import Product, OffendingItem
from .headers import HEADERS, MOBILE_HEADERS
from .linkchecker_engine import LinkChecker, product_initializer

import urllib3
import random
import time
import os


OFFENDING_LINKS = ['https://torrentz.eu/73874c321d1a60b088382c36c3d7a34105d23f30',
                   'https://torrentz.eu/4b0e3ddbdfd4851eb966ad9009f3d99ef7b42464',
                   'https://torrentz.eu/1c6b4d81b4ec3e21eff880a5c11ceb1f0756e3a1',
                   'https://torrentz.eu/89526fa3e1fb76135a780a2fde67f94f8cce19b5',
                   'https://torrentz.eu/85551b02354f7f7b153cdc7237400d953e7bc880',
                   'https://torrentz.eu/4b0e3ddbdfd4851eb966ad9009f3d99ef7b42464',
                   "http://www.bittorrent.com/73874c321d1a60b0883823d99ef7b42462",
                   "http://www.bittorrent.com/354f7f7b153cdc7823009fd99ef7b42462",
                   ]


class DatabaseConnectionTestCase(TestCase):
    # Testing on database as well as data manipulation in
    # isolation (interacting with db models, performing crud etc.)

    def test_database_connection(self):
        # In order to do database check & test interaction,
        # I will perform simple CRUD on already existing models
        product_title = "Torrent Killer"
        Product.objects.create(title=product_title)
        # simple query to check if we get what is expected
        query = Product.objects.last()
        self.assertEqual(query.title, product_title)

    def test_product_name_query_and_initialization_if_needed(self):
        # In this case we need to write a small helper function,
        # that will check if product for given title exists or not.
        # If not then it will be initialized (we can't waste our link
        # check, right?)
        title = "Amazing App"

        def product_initializer(product_title):
            try:
                return Product.objects.get(title=product_title)
            except ObjectDoesNotExist:
                Product.objects.create(title=product_title)
                return Product.objects.get(title=product_title)
        value = product_initializer(title)
        # And finally, the result should be as we expect, so the title
        # provided at the beginning of the test. Woohoo?
        self.assertEqual(value.title, title)

    def test_on_storing_results_from_link_checker_in_the_database(self):
        # As the test method says, we are going to use what we acquired
        # before, so class method that gives us valid data and we will
        # store it in the database
        application_title = "SecureBot"
        checked_links = LinkChecker(application_title)
        checked_links.get_links(OFFENDING_LINKS)
        # This time we kind of assume that title comes as a user input,
        # that's crucial for the next step, which is Product object
        # initialization
        product_details = product_initializer(application_title)
        # What we are doing here is super simple, just look
        for key, value in checked_links.links.items():
            # We have to different status codes, so like:
            if key == 1:
                for url in value:
                    OffendingItem.objects.create(
                        # And here is where our helper is helping!
                        product=product_details,
                        # Those details are received simply from checked_links
                        url=url,
                        status=key,
                    )
            if key == 0:
                for url in value:
                    OffendingItem.objects.create(
                        # You have explanation above!
                        product=product_details,
                        url=url,
                        status=key,
                    )
        # That's a lot, but there was no need for checking those elements
        # in separation, since they do exactly the same, and we are not
        # testing python here (aren't we?). Anyway!
        for dead_links in OffendingItem.objects.filter(status=0):
            self.assertIn(dead_links.url, OFFENDING_LINKS[5:])
            self.assertEqual(dead_links.product_id, product_details.id)
        # And yet another assertion (to confirm our logic)
        for live_links in OffendingItem.objects.filter(status=1):
            self.assertIn(live_links.url, OFFENDING_LINKS[:5])
            self.assertEqual(live_links.product_id, product_details.id)


class LinkCheckerModuleTestCase(TestCase):
    # Test case just about our link-checking module, which will
    # act as a main engine of our app and will be integrated with
    # manage commands
    checked_links = LinkChecker("SecureBot")

    def test_initialize_LinkChecker_object(self):
        # The plan is to make our app engine as a class, so let's
        # get into it. Class should have getter and data "injector"
        # that will append results into the database. Also, it should
        # get an argument on initialization, which is product title,
        # since OffendingItem has a foreign key relationship with Product
        # model.
        self.assertEqual(self.checked_links.application_title, "SecureBot")
        # Let's use this occasion to bind second item on object
        # initialization, and it's going to be dictionary with keys
        # corresponding to our data model structure (0 and 1, empty
        # lists as values. Why? Easy to append something!)
        self.assertTrue(isinstance(self.checked_links.links, dict))

    def test_get_links_and_read_response_status_200(self):
        # For the purpose of reading response status we will need to
        # use urllib3 library, because it's good. We know that in order
        # to make app run smoothly, we will need to silence warning (ssl
        # connection security warning).
        urllib3.disable_warnings()
        # First, checking for status 200
        http = urllib3.PoolManager()
        for url in OFFENDING_LINKS:
            response = http.request('GET', url)
            if response.status == 200:
                self.checked_links.links[1].append(url)
        # We know that we should get 6 items appended, but let's see...
        self.assertEqual(len(self.checked_links.links[1]), 6)

    def test_get_links_and_read_response_status_404(self):
        # Ok we can move into tests on items with response code 404!
        urllib3.disable_warnings()
        http = urllib3.PoolManager()
        for url in OFFENDING_LINKS:
            response = http.request('GET', url)
            if response.status == 200:
                self.checked_links.links[1].append(url)
            elif response.status == 404:
                self.checked_links.links[0].append(url)
        # Ok, if that was smart enough then we should get two dead
        # links and that equates len 2 on dictionary with key 0
        self.assertEqual(len(self.checked_links.links[0]), 2)

    def test_particular_content_in_http_response(self):
        # This is how beauty of TTD helped us with code composition:
        # our third condition made us adding some new condition into
        # the if/elif statement
        urllib3.disable_warnings()
        http = urllib3.PoolManager()
        header = {
            "User-Agent":
            "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"
        }
        for url in OFFENDING_LINKS:
            try:
                response = http.request('GET', url, headers=header)
                if (response.status == 200 and
                        "copyright complaint" not in str(response.data)):
                    self.checked_links.links[1].append(url)
                elif (response.status == 200 and
                        "copyright complaint" in str(response.data)):
                    self.checked_links.links[0].append(url)
                elif response.status == 404:
                    self.checked_links.links[0].append(url)
            # If the link is broken and cannot return any response, we need to
            # handle it with exception provided by urllib3. What does it mean that
            # we can't get any http response? The link is dead!
            except urllib3.exceptions.MaxRetryError:
                self.checked_links.links[0].append(url)
        # OK, so as far as we know (don't ask), none of those links above
        # should not contain mentioned content, so following test should pass
        for item in self.checked_links.links[1]:
            self.assertIn("https://", item)
        self.assertEqual(len(self.checked_links.links[1]), 6)
        # And that's easier one
        self.assertTrue(len(self.checked_links.links[0]) == 2)

    def test_adding_random_time_intervals_and_more_headers_for_requests(self):
        # There is a need to implement a random time intervals between
        # sending requests, especially when requesting from technically
        # same place, and adding some more headers that we can choose from
        # randomly. Here's random case twice, so let's get into it.
        urllib3.disable_warnings()
        http = urllib3.PoolManager()
        # We are going to make it crazy and pull 80 requests. That should
        # prove us whether the way of doing the thins is right.
        # UPDATE
        # We are also going to shuffle our list to make it even more random
        # (in case we are scraping from multiple different sources)
        random.shuffle(OFFENDING_LINKS)
        for times in range(10):
            for url in OFFENDING_LINKS:
                # So that's first change, we push requests in range between
                # 0 and 1.1 second (quit unusual, to make the whole process
                # less "robotic" and more "human"
                time.sleep(random.uniform(0, 1.1))
                try:
                    response = http.request(
                        'GET',
                        url,
                        # Here's the second implementation. There are only couple
                        # of headers stored in HEADERS variable, but that's ok for
                        # now and it can be improved by adding more
                        headers=HEADERS[random.randrange(len(HEADERS))],
                    )
                    if (response.status == 200 and
                            "copyright complaint" not in str(response.data)):
                        self.checked_links.links[1].append(url)
                    elif (response.status == 200 and
                            "copyright complaint" in str(response.data)):
                        self.checked_links.links[0].append(url)
                    elif response.status == 404:
                        self.checked_links.links[0].append(url)
                except urllib3.exceptions.MaxRetryError:
                    self.checked_links.links[0].append(url)
        # Checks to see if we have around 60 (will use AlmostEqual
        # in this case) live links. Check!
        self.assertAlmostEqual(len(self.checked_links.links[1]), 60)

    def test_different_approach_on_headers_with_http_request(self):
        # Trying different approach to get better results, so implement random
        # headers every time http object is initialized
        urllib3.disable_warnings()
        random.shuffle(OFFENDING_LINKS)
        for times in range(10):
            http = urllib3.PoolManager()
            for url in OFFENDING_LINKS:
                # The rest of the code stays almost the same
                time.sleep(random.uniform(0, 1.2))
                try:
                    response = http.request(
                        'GET',
                        url,
                        headers=HEADERS[random.randrange(len(HEADERS))],
                    )
                    if (response.status == 200 and
                            "copyright complaint" not in str(response.data)):
                        self.checked_links.links[1].append(url)
                    elif (response.status == 200 and
                            "copyright complaint" in str(response.data)):
                        self.checked_links.links[0].append(url)
                    elif response.status == 404:
                        self.checked_links.links[0].append(url)
                except urllib3.exceptions.MaxRetryError:
                    # OK. On random range 1.1 second between requests we have
                    # from 41 to 47 requests ok, and the rest is rubbish. I am
                    # going to fake mobile headers on exception (it should be still
                    # faster than 110s.), and what is not being read, will be stored
                    # in log file, so can be checked again
                    #
                    # DON'T WORRY, if successful, this code will be shortened!
                    try:
                        time.sleep(random.uniform(0, 1.2))
                        response = http.request(
                            'GET',
                            url,
                            headers=MOBILE_HEADERS[random.randrange(len(MOBILE_HEADERS))],
                        )
                        if (response.status == 200 and
                                "copyright complaint" not in str(response.data)):
                            self.checked_links.links[1].append(url)
                        elif (response.status == 200 and
                                "copyright complaint" in str(response.data)):
                            self.checked_links.links[0].append(url)
                        elif response.status == 404:
                            self.checked_links.links[0].append(url)
                    except urllib3.exceptions.MaxRetryError:
                        current_path = os.getcwd()
                        log_dir = "/linkchecker_log/"
                        file_name = "log.txt"
                        with open(os.path.join(current_path+log_dir+file_name), "w") as wr:
                            wr.write(url)
                            wr.write("\n")
        # Check!
        self.assertAlmostEqual(len(self.checked_links.links[1]), 60)


class ReadyEngineTestCase(TestCase):

    def test_whole_engine_from_import(self):
        # As the test name says we are going to make a simple test
        # to check if there was no typo or anything
        test_links = LinkChecker("Torqll")
        test_links.get_links(OFFENDING_LINKS)
        # We are using similar assertions as before
        self.assertAlmostEqual(len(test_links.links[1]), 6)
        self.assertAlmostEqual(len(test_links.links[0]), 2)
        test_links.set_data()
        counter = 0
        for counting_items in OffendingItem.objects.all():
            counter += 1
        # And this should be equal, if it is then we are done
        self.assertEqual(
            counter, (len(test_links.links[1]) + len(test_links.links[0]))
        )
