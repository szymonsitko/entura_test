from django.core.exceptions import ObjectDoesNotExist

from .headers import HEADERS
from .models import Product, OffendingItem

import os
import random
import urllib3
import time


def product_initializer(product_title):
    # Simple function-helper to query database before
    # new product initialization. Uses try/except block
    # because of ObjectDoesNotExist exception
    try:
        return Product.objects.get(title=product_title)
    except ObjectDoesNotExist:
        Product.objects.create(title=product_title)
        return Product.objects.get(title=product_title)


class LinkChecker(object):
    # Class structure is the best construction for this task

    def __init__(self, application_title):
        self.application_title = application_title
        self.links = dict({0: [], 1: []})

    def get_links(self, offending_links):
        # Disabling warnings in order to avoid any exceptions
        # but also for keeping app running in a 'clean' way
        urllib3.disable_warnings()
        http = urllib3.PoolManager()
        random.shuffle(offending_links)
        for url in offending_links:
            try:
                # Time 'gap' and random pool implemented, in order
                # to fake humans interaction with urls when scraping
                time.sleep(random.uniform(0, 1.6))
                response = http.request(
                    'GET',
                    url,
                    # We are using list of headers stored externally
                    # which can be always extended (recommended)
                    headers=HEADERS[random.randrange(len(HEADERS))],
                )
                if (response.status == 200 and
                        "copyright complaint" not in str(response.data)):
                    self.links[1].append(url)
                elif (response.status == 200 and
                        "copyright complaint" in str(response.data)):
                    self.links[0].append(url)
                elif response.status == 404:
                    self.links[0].append(url)
            # If getting exception, instead of classifying link as 404 (dead)
            # which could be not true, we are giving it a chance to be checked again
            except urllib3.exceptions.MaxRetryError:
                file_name = "linkchecker_log.txt"
                with open(os.path.join(file_name), "w") as wr:
                    wr.write(url)
                    wr.write("\n")

    def set_data(self):
        # This structure is simple, we are using our helper defined earlier
        # and then make simple iteration in order to insert data into the database.
        # According to app data model, time will be defined as default (timestamp)
        product_details = product_initializer(self.application_title)
        for key, value in self.links.items():
            if key == 1:
                for url in value:
                    OffendingItem.objects.create(
                        product=product_details,
                        url=url,
                        status=key,
                    )
            if key == 0:
                for url in value:
                    OffendingItem.objects.create(
                        product=product_details,
                        url=url,
                        status=key,
                    )
